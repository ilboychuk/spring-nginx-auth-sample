package i.am.teapot.nginx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

/**
 * @author ilya.boychuk
 *         Created: 25.05.2017
 */
@RestController
@SpringBootApplication
public class AuthApplication {
    private final static Logger log = LoggerFactory.getLogger(AuthApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class);
    }

    @GetMapping
    public String data() {
        log.info("Data request.");
        return UUID.randomUUID().toString();
    }

    @GetMapping(path = "auth")
    public ResponseEntity<String> auth(@RequestHeader(defaultValue = "200") Integer code, @RequestHeader(defaultValue = "Ok") String message) {
        log.info("Auth request: Code {}. Message {}", code, message);

        return ResponseEntity.status(code)
                .header("X-AUTH-CODE", code.toString())
                .header("X-AUTH-MESSAGE", message)
                .body(String.format("Code %s. Message %s", code, message));
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.I_AM_A_TEAPOT)
    public String throwable(Throwable t) {
        log.error("Throw {}", t.getMessage(), t);
        return "i.am.a.teapot";
    }

}
