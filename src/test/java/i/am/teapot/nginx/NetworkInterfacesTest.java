package i.am.teapot.nginx;

import org.junit.Test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author ilya.boychuk
 *         Created: 29.05.2017
 */
public class NetworkInterfacesTest {

    private final static List<String> inetInterfaces = Arrays.asList("docker0", "en0", "eno1", "eno0");

    @Test
    public void retrieveDockerBridgeIp() throws Exception {
        String dockerBridgeId = Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
                .filter(network -> inetInterfaces.contains(network.getDisplayName()))
                .flatMap(network -> Collections.list(network.getInetAddresses()).stream())
                .filter(ia -> ia instanceof Inet4Address)
                .map(InetAddress::getHostAddress).findFirst()
                .orElse(null);
        System.out.println(dockerBridgeId);
    }
}
