package i.am.teapot.nginx;

import com.jayway.jsonpath.JsonPath;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;

import java.net.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author ilya.boychuk
 * Created: 25.05.2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, properties = "server.port=8080")
public class AuthApplicationTest {
    private final static Logger log = LoggerFactory.getLogger(AuthApplicationTest.class);

    private final static String dockerBridgeIP;
    private final static List<String> inetInterfaces = Arrays.asList("docker0", "en0", "eno1", "eno0", "eth0");

    static {
        try {
            dockerBridgeIP = Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
                    .filter(network -> inetInterfaces.contains(network.getDisplayName()))
                    .flatMap(network -> Collections.list(network.getInetAddresses()).stream())
                    .filter(ia -> ia instanceof Inet4Address)
                    .map(InetAddress::getHostAddress).findFirst()
                    .orElseThrow(() -> new IllegalStateException("Docker bridge interface not found"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @ClassRule
    public final static GenericContainer nginx = new GenericContainer("nginx:1.10.0")
            .withClasspathResourceMapping("nginx.conf", "/etc/nginx/nginx.conf", BindMode.READ_WRITE)
            .withExtraHost("app-host", dockerBridgeIP) //bridge ip
            .withExposedPorts(80);


    private final static TestRestTemplate restTemplate = new TestRestTemplate();

    private URI baseUrl() throws MalformedURLException {
        return URI.create("http://" + nginx.getContainerIpAddress() + ":" + nginx.getMappedPort(80));
    }

    @Test
    public void successRequestTest() throws Exception {

        RequestEntity<Void> request = RequestEntity.get(baseUrl())
                .header("code", "200").header("message", "ok request").build();
        log.info("Request: {}", request);

        ResponseEntity<String> response = restTemplate.exchange(request, String.class);
        log.info("Response: {}", response);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void badRequestTest() throws Exception {

        RequestEntity<Void> request = RequestEntity.get(baseUrl())
                .header("code", "400").header("message", "bad request").build();
        log.info("Request: {}", request);


        ResponseEntity<String> response = restTemplate.exchange(request, String.class);
        log.info("Response: {}", response);

        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        Assert.assertEquals((Integer) 400, JsonPath.read(response.getBody(), "$.code"));
        Assert.assertEquals("bad request", JsonPath.read(response.getBody(), "$.message"));
    }


    @Test
    public void systemErrorTest() throws Exception {

        RequestEntity<Void> request = RequestEntity.get(baseUrl())
                .header("code", "500").header("message", "system error").build();
        log.info("Request: {}", request);

        ResponseEntity<String> response = restTemplate.exchange(request, String.class);
        log.info("Response: {}", response);

        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        Assert.assertEquals((Integer) 500, JsonPath.read(response.getBody(), "$.code"));
        Assert.assertEquals("system error", JsonPath.read(response.getBody(), "$.message"));
    }


}
